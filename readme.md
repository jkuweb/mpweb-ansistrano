# ![La Salle BES](http://jcarreras.es/images/lasalle.png)

# ![screenshot](.screenshot.png)

# Descripción
-----------------------

Práctica para aprender a usar Ansistrano.



# Instalación
-----------------------

```
$ vagrant up
```


# Instrucciones
-----------------------

- Mira el contenido del fichero `deploy/deploy.yml` para entender la configuración que se ha hecho para hacer los pasos a producción.
- Ejecuta el siguiente comando en tu host: `ansible-playbook --private-key .vagrant/machines/default/virtualbox/private_key -u vagrant -i 1.2.3.4, deploy/deploy.yml`
- Entra en `http://1.2.3.4/` en tu navegador
- Verás que se ha hecho paso a producción del contenido de /src
- Cambia el fichero /src/index.html
- Ejecuta otra vez un paso a producción con: `ansible-playbook --private-key .vagrant/machines/default/virtualbox/private_key -u vagrant -i 1.2.3.4, deploy/deploy.yml`
- Mira que en `http://1.2.3.4/` la web haya cambiado con los cambios que hayas hecho
- Entra en la máquina de vagrant y mira el directorio `/var/www/html/` y analiza el contenido del directorio


# Desinstalación
-----------------------

```
$ vagrant destroy
```
